<p><span style="font-weight: 400;">Source: https://gitlab.com/somkundu1989/basic-crud-using-customphp</span></p>
<p><span style="font-weight: 400;">download link is there to download as zip</span></p>
<p><span style="font-weight: 400;">in case you download; extract it to xampp/htdocs or wamp/www or somewhere you specified to run your php projects</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">-----------------------------------------------------------------------------------</span></p>
<p><span style="font-weight: 400;">How to run:</span></p>
<p><span style="font-weight: 400;">/academic_year ( http://localhost/basic-crud-using-customphp/academic_year)</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">------------------------------------------------------------------------------------</span></p>
<p><span style="font-weight: 400;">What to learn:</span></p>
<ol>
<li><span style="font-weight: 400;"> env file -&gt; which is having </span><span style="font-weight: 400;"><br /><br /></span></li>
</ol>
<p><span style="font-weight: 400;">&lt;?php</span></p>
<p><span style="font-weight: 400;">$base_url='http://localhost/making-layout-using-customphp/';</span></p>
<p><span style="font-weight: 400;">$asset_url=$base_url.'public/admin/';</span></p>
<p><span style="font-weight: 400;">$public_url=$base_url.'public/';</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">$servername = "localhost";</span></p>
<p><span style="font-weight: 400;">$username = "root";</span></p>
<p><span style="font-weight: 400;">$password = "";</span></p>
<p><span style="font-weight: 400;">$dbname = "virtual_school";</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">env file can be renamed with anything you like&nbsp;</span></p>
<p><span style="font-weight: 400;">-------------------------------------------------------------------------------------------------------------------</span></p>
<ol start="2">
<li><span style="font-weight: 400;"> Academic_year folder has 3 files</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">action.php -&gt; bassically in respective to CI Framework it can be considered as controller &amp; model&nbsp;</span></li>
</ol>
<p><span style="font-weight: 400;">Index.php -&gt; is holding the table and in the top includes action.php</span></p>
<p><span style="font-weight: 400;">Addoredit.php -&gt; is holding the form part and getting displayed if&nbsp;</span></p>
<p><span style="font-weight: 400;">&lt;?php if(!empty($_GET) &amp;&amp; ($_GET['action']=='add' || $_GET['action']=='edit')){ ?&gt;</span></p>
<p><span style="font-weight: 400;">Mentioned in line no: 27 of index.php</span></p>
<p><span style="font-weight: 400;">--------------------------------------------------------------------------------------------------------------------</span></p>
<ol start="3">
<li><span style="font-weight: 400;"> illustration of action.php</span></li>
</ol>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">// line 2 : includes db_connection.php</span></p>
<p><span style="font-weight: 400;">// line 4-11 : get all rows and stores into an array (as an array of objects) namely $allRecords which is further used in index.php to create table rows&nbsp;</span></p>
<p><span style="font-weight: 400;">if($_GET){</span></p>
<p><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp;&nbsp;if($_GET['action']=='delete'){</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp;</span><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp; </span><span style="font-weight: 400;"> // delete action performed</span></p>
<p><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp;&nbsp;}</span></p>
<p><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp;&nbsp;if($_GET['action']=='edit'){</span></p>
<p><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp;</span><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp; </span><span style="font-weight: 400;"> //data getting fetched by where $_GET['id']</span></p>
<p><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp;&nbsp;}</span></p>
<p><span style="font-weight: 400;">}</span></p>
<p><span style="font-weight: 400;">if($_POST){</span></p>
<p><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp;// used to handle store/update&nbsp;&nbsp;</span></p>
<p><span style="font-weight: 400;">}</span></p>
<p><br /><br /></p>
<p><span style="font-weight: 400;">and&nbsp;</span></p>
<p><br /><br /></p>
<p><span style="font-weight: 400;">header('Location: '.$base_url.'academic_year/');&nbsp;</span></p>
<p><span style="font-weight: 400;">means: redirect to url &ldquo;&lt;base-url&gt;/academic_year&rdquo; that's it.</span></p>
<p>&nbsp;</p>
<p>&nbsp;---------------------------------------------------------------------------------</p>
<p><span style="font-weight: 400;">&lt;?=(!empty($data))?$data-&gt;id :''?&gt;<br />&lt;?php</span></p>
<p><span style="font-weight: 400;">if(!empty($data)){ </span></p>
<p><span style="font-weight: 400;">&nbsp; &nbsp; &nbsp; &nbsp;echo $data-&gt;id;</span></p>
<p><span style="font-weight: 400;">}else{ </span></p>
<p><span style="font-weight: 400;">&nbsp; &nbsp; &nbsp; &nbsp;echo '';</span></p>
<p><span style="font-weight: 400;">}</span></p>
<p><span style="font-weight: 400;">?&gt;</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br /><br /><br /><br /></p>