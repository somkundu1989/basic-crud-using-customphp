<?php
include '../db_connection.php';

$sql = "SELECT * FROM academicsessions WHERE deleted_at IS NULL";	
$result = $conn->query($sql);
$allRecords=[];
if ($result->num_rows > 0) {
	while ($obj = $result->fetch_object()) {
		$allRecords[]=$obj;
	}
}
if($_GET){
	if($_GET['action']=='delete'){

		$sql = "DELETE FROM academicsessions WHERE id=".$_GET['id'];
		if ($conn->query($sql) === TRUE) {
		    echo "Record deleted successfully";
		} else {
		    echo "Error deleting record: " . $conn->error;
		}
		header('Location: '.$base_url.'academic_year/');
	}
	if($_GET['action']=='edit'){
		$sql = "SELECT * FROM academicsessions WHERE id=".$_GET['id'];
		$result = $conn->query($sql);
		$data=null;
		if ($result->num_rows == 1) {
			$data = $result->fetch_object();
		}
	}
}
if($_POST){
	$sql = "INSERT INTO academicsessions(from_date, to_date, title, slug) VALUES ('".$_POST['from_date']."', '".$_POST['to_date']."', '".$_POST['title']."', '".$_POST['slug']."')";
	if($_POST['record_id']>0){
		$sql = "UPDATE academicsessions SET from_date='".$_POST['from_date']."',to_date='".$_POST['to_date']."',title='".$_POST['title']."',slug='".$_POST['slug']."'  WHERE id=".$_POST['record_id'];
	}

	if ($conn->query($sql) === TRUE) {
	    echo "Record updated successfully";
	} else {
	    echo "Error updating record: " . $conn->error;
	}
	header('Location: '.$base_url.'academic_year/');
}