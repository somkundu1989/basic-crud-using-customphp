<div class="card card-default">
  <div class="card-header">
    <h3 class="card-title"><?=$_GET['action']?> Academic Years</h3>

  </div>
  <!-- /.card-header -->
  <form class="form-horizontal" method="post" action="">
    <input type="hidden" name="record_id" value="<?=(!empty($data))?$data->id :''?>">
    <div class="card-body">

      <div class="row">
        <div class="col-sm-6 form-group">
          <label>From Date</label>
          <input type="text" class="form-control" name="from_date" value="<?=(!empty($data))?$data->from_date :''?>">
        </div>
        <div class="col-sm-6 form-group">
          <label>To Date</label>
          <input type="text" class="form-control" name="to_date" value="<?=(!empty($data))?$data->to_date :''?>">
        </div>
        <div class="col-sm-6 form-group">
          <label>Title</label>
          <input type="text" class="form-control" name="title" value="<?=(!empty($data))?$data->title :''?>">
        </div>
        <div class="col-sm-6 form-group">
          <label>Slug</label>
          <input type="text" class="form-control" name="slug" value="<?=(!empty($data))?$data->slug :''?>">
        </div>
      </div>

        
    <!-- /.row -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="submit" class="btn btn-info">Submit</button>
      <a href="<?=$base_url?>academic_year" class="btn btn-default">Cancel</a>
    </div>
  </form>
</div>