-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 09, 2020 at 08:58 PM
-- Server version: 8.0.20-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `virtualschool`
--
CREATE DATABASE IF NOT EXISTS `virtualschool` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `virtualschool`;

-- --------------------------------------------------------

--
-- Table structure for table `academicsessions`
--

CREATE TABLE `academicsessions` (
  `id` int UNSIGNED NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `is_current` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: active, 0: inactive	',
  `is_next` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: active, 0: inactive	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academicsessions`
--

INSERT INTO `academicsessions` (`id`, `from_date`, `to_date`, `title`, `slug`, `is_current`, `is_next`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2020-02-01', '2021-01-31', '2020 2021', '2020-2021', 1, 0, '2019-10-09 06:55:03', '2020-04-29 21:59:36', NULL),
(2, '2021-02-01', '2022-01-31', '2021-2022', '2021-2022', 0, 1, '2019-10-09 06:55:45', '2020-04-29 22:00:15', NULL),
(6, '2021-02-01', '2021-02-01', '2021-2022', '2021-2022-1', 0, 1, '2019-10-09 06:55:45', '2019-12-18 01:42:11', '2020-03-08 06:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `academicsession_grade_subjects`
--

CREATE TABLE `academicsession_grade_subjects` (
  `id` bigint UNSIGNED NOT NULL,
  `session_id` int UNSIGNED NOT NULL,
  `grade_id` int UNSIGNED NOT NULL,
  `subject_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `academicsession_grade_subjects`
--

INSERT INTO `academicsession_grade_subjects` (`id`, `session_id`, `grade_id`, `subject_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, '2020-04-29 12:38:13', '2020-04-30 01:45:33', NULL),
(2, 1, 1, 3, '2020-04-29 12:38:26', '2020-04-30 01:17:35', NULL),
(3, 1, 1, 3, '2020-04-29 12:40:13', '2020-04-29 12:40:13', NULL),
(4, 1, 2, 2, '2020-04-29 12:40:27', '2020-04-29 12:40:27', NULL),
(5, 1, 2, 3, '2020-04-29 12:40:38', '2020-04-29 12:40:38', NULL),
(6, 1, 2, 4, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(7, 1, 3, 2, '2020-04-29 12:40:27', '2020-04-29 12:40:27', NULL),
(8, 1, 3, 3, '2020-04-29 12:40:38', '2020-04-29 12:40:38', NULL),
(9, 1, 3, 4, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(10, 1, 3, 7, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(11, 1, 4, 2, '2020-04-29 12:40:27', '2020-04-29 12:40:27', NULL),
(12, 1, 4, 3, '2020-04-29 12:40:38', '2020-04-29 12:40:38', NULL),
(13, 1, 4, 4, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(14, 1, 4, 7, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(15, 1, 4, 5, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(16, 1, 4, 6, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(17, 1, 5, 2, '2020-04-29 12:40:27', '2020-04-30 01:44:04', NULL),
(18, 1, 5, 3, '2020-04-29 12:40:38', '2020-04-30 01:44:16', NULL),
(19, 1, 5, 4, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(20, 1, 5, 7, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(21, 1, 5, 5, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(22, 1, 5, 6, '2020-04-29 12:40:47', '2020-04-29 12:40:47', NULL),
(23, 1, 5, 8, '2020-04-29 13:47:39', '2020-04-29 13:47:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int UNSIGNED NOT NULL,
  `session_id` int UNSIGNED DEFAULT NULL,
  `grade_id` int UNSIGNED DEFAULT NULL,
  `subject_id` int UNSIGNED DEFAULT NULL,
  `teacher_id` bigint UNSIGNED DEFAULT NULL,
  `classsession_id` int UNSIGNED DEFAULT NULL,
  `last_submission_date` varchar(20) DEFAULT NULL,
  `instructions` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `attachments` text,
  `total_marks` float(5,2) NOT NULL DEFAULT '100.00',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: drafted, 1: complete',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_attachments`
--

CREATE TABLE `assignment_attachments` (
  `id` int UNSIGNED NOT NULL,
  `assignment_id` int UNSIGNED DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_students`
--

CREATE TABLE `assignment_students` (
  `id` int UNSIGNED NOT NULL,
  `assignment_id` int UNSIGNED DEFAULT NULL,
  `student_id` bigint UNSIGNED DEFAULT NULL,
  `answer` text,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:pending, 1: submitted, 2:drafted, 3: reviewed-by-teacher-or-tutor',
  `marks` float(5,2) DEFAULT NULL,
  `submitted_at` varchar(20) DEFAULT NULL,
  `evaluated_at` varchar(20) DEFAULT NULL,
  `remarks` text,
  `marks_per_questions` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_student_attachments`
--

CREATE TABLE `assignment_student_attachments` (
  `id` int UNSIGNED NOT NULL,
  `assignment_student_id` int UNSIGNED DEFAULT NULL,
  `attachment` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `classsessions`
--

CREATE TABLE `classsessions` (
  `id` int UNSIGNED NOT NULL,
  `session_id` int UNSIGNED DEFAULT NULL,
  `grade_id` int UNSIGNED DEFAULT NULL,
  `subject_id` int UNSIGNED DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `teacher_id` bigint UNSIGNED DEFAULT NULL,
  `scheduled_date` date DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `chat_or_call_external_link` varchar(255) DEFAULT NULL,
  `chat_or_call_saved_link` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: setup-need-to-complete,1: setup-complete, 2: class-passed/completed, 3: class-cancelled, 4: started',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classsessions`
--

INSERT INTO `classsessions` (`id`, `session_id`, `grade_id`, `subject_id`, `start_time`, `end_time`, `teacher_id`, `scheduled_date`, `title`, `slug`, `description`, `chat_or_call_external_link`, `chat_or_call_saved_link`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 2, '2020-04-30 20:00:00', '2020-04-30 20:30:00', 3, '2020-04-16', 'Bseen Pool', NULL, 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 'https://www.youtube.com/watch?v=invCwBJwFXY', NULL, 1, '2020-04-29 22:19:59', '2020-04-29 23:05:16', NULL),
(2, 1, 2, 3, '2020-04-30 20:00:00', '2020-04-30 20:30:00', 2, '2020-05-01', 'English Intro to Lesson 1', NULL, NULL, 'https://www.youtube.com/watch?v=invCwBJwFXY', NULL, 4, '2020-04-29 23:04:48', '2020-04-29 23:25:06', NULL),
(3, 1, 1, 3, '2020-04-30 20:30:00', '2020-04-30 20:00:00', 2, '2020-05-01', 'English Intro to Lesson 1.1', NULL, 'their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like.', 'https://www.youtube.com/watch?v=F7GM3iFJl94', 'http://www.kozey.com/as4dasd5rt9uy5tuup8oui7o-kasuddab-jasdhghc0g', 4, '2020-04-29 23:38:48', '2020-04-29 23:43:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `classsession_attendance_review`
--

CREATE TABLE `classsession_attendance_review` (
  `id` int UNSIGNED NOT NULL,
  `classsession_id` int UNSIGNED DEFAULT NULL,
  `student_id` bigint UNSIGNED DEFAULT NULL,
  `is_attended` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: attended, 0: not-attended',
  `rating` tinyint(1) DEFAULT '0' COMMENT '1 to 5',
  `comment` text,
  `report_date` date DEFAULT NULL,
  `report_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `classsession_requests`
--

CREATE TABLE `classsession_requests` (
  `id` int UNSIGNED NOT NULL,
  `teacher_id` bigint UNSIGNED DEFAULT NULL,
  `grade_id` int UNSIGNED DEFAULT NULL,
  `subject_id` int UNSIGNED DEFAULT NULL,
  `session_id` int UNSIGNED DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `scheduled_date` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:pending, 1: approved, 2:rejected',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classsession_requests`
--

INSERT INTO `classsession_requests` (`id`, `teacher_id`, `grade_id`, `subject_id`, `session_id`, `title`, `scheduled_date`, `status`, `start_time`, `end_time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 5, 5, 1, 'Univ A 2019 2020', '2020-05-02', 0, '2020-04-30 13:14:00', '2020-04-30 15:15:00', '2020-04-30 02:19:22', '2020-04-30 03:55:06', NULL),
(2, 3, 1, 1, 1, 'Univ A 2019 2020', '2020-05-01', 0, '2020-04-30 19:06:00', '2020-04-30 22:06:00', '2020-04-30 10:06:18', '2020-04-30 10:06:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `classsession_request_students`
--

CREATE TABLE `classsession_request_students` (
  `id` int UNSIGNED NOT NULL,
  `classsession_request_id` int UNSIGNED DEFAULT NULL,
  `student_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `title`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Grade 1', 'grade-1', '2020-04-29 10:50:00', '2020-04-29 10:50:00', NULL),
(2, 'Grade 2', 'grade-2', '2020-04-29 10:50:13', '2020-04-29 10:50:13', NULL),
(3, 'Grade 3', 'grade-3', '2020-04-29 10:50:27', '2020-04-29 10:50:27', NULL),
(4, 'Grade 4', 'grade-4', '2020-04-29 10:50:39', '2020-04-29 10:50:39', NULL),
(5, 'Grade 5', 'grade-5', '2020-04-29 10:50:51', '2020-04-29 10:51:04', NULL),
(6, 'Grade 6', 'grade-6', '2020-04-29 13:41:29', '2020-04-29 13:41:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int UNSIGNED NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `footer_logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `establishment_date` date DEFAULT NULL,
  `top_contact_no` varchar(255) DEFAULT NULL,
  `footer_contact_no` varchar(255) DEFAULT NULL,
  `footer_fax_no` varchar(255) DEFAULT NULL,
  `footer_email_1` varchar(255) DEFAULT NULL,
  `footer_email_2` varchar(255) DEFAULT NULL,
  `footer_address` varchar(255) DEFAULT NULL,
  `copyright_text` varchar(255) DEFAULT NULL,
  `venture_1_title` varchar(255) DEFAULT NULL,
  `venture_1_url` varchar(255) DEFAULT NULL,
  `venture_2_title` varchar(255) DEFAULT NULL,
  `venture_2_url` varchar(255) DEFAULT NULL,
  `venture_3_title` varchar(255) DEFAULT NULL,
  `venture_3_url` varchar(255) DEFAULT NULL,
  `venture_4_title` varchar(255) DEFAULT NULL,
  `venture_4_url` varchar(255) DEFAULT NULL,
  `facebook_link` varchar(255) DEFAULT NULL,
  `twitter_link` varchar(255) DEFAULT NULL,
  `google_link` varchar(255) DEFAULT NULL,
  `latitude` varchar(10) DEFAULT NULL,
  `longitude` varchar(10) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `logo`, `footer_logo`, `favicon`, `title`, `meta_title`, `meta_description`, `meta_keywords`, `establishment_date`, `top_contact_no`, `footer_contact_no`, `footer_fax_no`, `footer_email_1`, `footer_email_2`, `footer_address`, `copyright_text`, `venture_1_title`, `venture_1_url`, `venture_2_title`, `venture_2_url`, `venture_3_title`, `venture_3_url`, `venture_4_title`, `venture_4_url`, `facebook_link`, `twitter_link`, `google_link`, `latitude`, `longitude`, `email_to`, `created_at`, `updated_at`) VALUES
(1, 'images/logo.png', 'images/logo-white.png', 'images/favicon.ico', 'Dynamic School', 'Dynamic School', 'Dynamic School', 'Dynamic School', '2015-05-15', '003 746 87 92', '+1 564 345-750-421', '+1 564 354-755-432', 'support@youremail.com', 'info@youremail.com', 'PO Box 16122 Collins Street West Victoria 8007 Australia', 'Crafted With <span class=\"heart\"></span> By Dynamic School', 'Venture 1', NULL, 'Venture 2', NULL, 'Venture 3', NULL, NULL, NULL, NULL, NULL, NULL, '22.6241504', '88.4306945', 'webdevsom@gmail.com,sahadipa339@gmail.com', '2020-04-11 17:16:27', '2020-04-11 17:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@vclass.com', '$2y$10$.6ms7/EbEcO/InhQRD3uO.g31jokxRKyZa1/dZrVqvSUABsOKv3F2', '2019-08-26 07:50:55'),
('sourav.dey@dreamztech.com', '$2y$10$mLA9OXUtb98lHn1c29hdxumxE1cJFjH6ZyxfJj7PZ7w76elJdzsR.', '2019-11-18 02:44:53'),
('tester@mail.com', '$2y$10$wVASumoZjIuqBIFdCcw4/.RNSePtwjQAdvG/hPiHU3s8vXztTK1rm', '2019-11-26 04:05:03'),
('bruce08@yopmail.com', '$2y$10$JiSwfIHPx9zKw/Kzk6K6U.cJQzc6VxtLh7Kon8Wdv.08WMFjD2lTu', '2019-12-10 07:26:20'),
('subhamnayak@yopmail.com', '$2y$10$iPFeTt63PFj8frEycra.h.yMbGIcS18LMEPRpJj9W.w0W9Fko7qj6', '2019-12-10 07:26:56'),
('sourav.dey@gmail.com', '$2y$10$iTavT0rSF4w06pppvx5rcuaMXX.jLl1LosbTfcB.dMkRBtpIBM5vK', '2019-12-10 08:47:14'),
('divakar.bagari@dreamztech.com', '$2y$10$m1zsqsMY4SBs.J1tz6ntyukB8SeC0NRKkkLSmDAi7tjkHPYUaDOua', '2019-12-24 05:25:39'),
('moumita.hazra@dreamztech.com', '$2y$10$yYoqTHiyiC6s.dbRz3eSbe3DCH9HEQcqO/CUYm26/bV5aTPkSgqYG', '2019-12-24 06:06:38'),
('admin@admin.com', '$2y$10$A5/qTqBPYFnt5aSdI.7tGeouBcT99voZSRlH4gAYOPbA8dsDd1WKS', '2019-12-24 06:34:41'),
('dhiman77@yopmail.com', '$2y$10$wGuqZNdR4uurx5lozPlF0uS8tfVlMXUBrRWLStFJgWlKXQCz1OkRm', '2019-12-30 10:04:42');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int UNSIGNED NOT NULL,
  `slug` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int UNSIGNED NOT NULL,
  `permission_id` int UNSIGNED DEFAULT NULL,
  `role_id` int UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int UNSIGNED NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'dev', 'Dev Admin', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(2, 'system-admin', 'System Administrator', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(5, 'teacher', 'Teachers', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(7, 'parent', 'Parents', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL),
(8, 'student', 'Students', '2019-10-02 05:05:27', '2019-10-02 05:05:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_grades`
--

CREATE TABLE `student_grades` (
  `id` int UNSIGNED NOT NULL,
  `student_id` bigint UNSIGNED DEFAULT NULL,
  `session_id` int UNSIGNED DEFAULT NULL,
  `grade_id` int UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_grades`
--

INSERT INTO `student_grades` (`id`, `student_id`, `session_id`, `grade_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, 1, NULL, NULL, NULL),
(2, 5, 1, 1, NULL, NULL, NULL),
(3, 6, 1, 1, NULL, NULL, NULL),
(4, 7, 1, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int UNSIGNED NOT NULL,
  `grade_id` int UNSIGNED DEFAULT NULL,
  `parent_id` int UNSIGNED DEFAULT NULL,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `is_academic` tinyint DEFAULT NULL COMMENT '1:yes, 0:no',
  `is_open` tinyint DEFAULT NULL COMMENT '1:yes, 0 No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `grade_id`, `parent_id`, `title`, `slug`, `is_academic`, `is_open`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, 'Morality and Values', 'morality-values', NULL, NULL, '2020-04-29 10:53:00', '2020-04-29 10:53:00', NULL),
(2, NULL, NULL, 'Mathematics', 'mathematics', NULL, NULL, '2020-04-29 11:01:54', '2020-04-29 11:01:54', NULL),
(3, NULL, NULL, 'First Language', 'first-language', NULL, NULL, '2020-04-29 11:02:23', '2020-04-29 11:02:23', NULL),
(4, NULL, NULL, 'Second Language', 'second-language', NULL, NULL, '2020-04-29 11:02:38', '2020-04-29 11:02:38', NULL),
(5, NULL, NULL, 'History', 'history', NULL, NULL, '2020-04-29 11:55:25', '2020-04-29 11:55:25', NULL),
(6, NULL, NULL, 'Geography', 'geography', NULL, NULL, '2020-04-29 11:55:39', '2020-04-29 11:55:39', NULL),
(7, NULL, NULL, 'Science', 'science', NULL, NULL, '2020-04-29 11:55:39', '2020-04-29 11:55:39', NULL),
(8, NULL, NULL, 'Social', 'social', NULL, NULL, '2020-04-29 13:43:44', '2020-04-29 13:43:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptionfees`
--

CREATE TABLE `subscriptionfees` (
  `id` int UNSIGNED NOT NULL,
  `session_id` int UNSIGNED DEFAULT NULL,
  `grade_id` int UNSIGNED DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `currency` varchar(5) NOT NULL DEFAULT 'zar',
  `frequency` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: One Time, 2: Recurrent',
  `is_activate` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active, 0: inactive	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptionfee_students`
--

CREATE TABLE `subscriptionfee_students` (
  `id` int UNSIGNED NOT NULL,
  `session_id` int UNSIGNED DEFAULT NULL,
  `grade_id` int UNSIGNED DEFAULT NULL,
  `subscription_fee_id` int UNSIGNED DEFAULT NULL,
  `paid_by` bigint UNSIGNED DEFAULT NULL,
  `student_id` bigint UNSIGNED DEFAULT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `currency` varchar(5) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `detail` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_subjects`
--

CREATE TABLE `teacher_subjects` (
  `id` int UNSIGNED NOT NULL,
  `teacher_id` bigint UNSIGNED NOT NULL,
  `subject_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `teacher_subjects`
--

INSERT INTO `teacher_subjects` (`id`, `teacher_id`, `subject_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `zipcode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `parent_id` bigint UNSIGNED DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_activate` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active, 0: inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `stripe_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `middle_name`, `name`, `email`, `contact_no`, `address`, `zipcode`, `date_of_joining`, `date_of_birth`, `parent_id`, `token`, `email_verified_at`, `password`, `api_token`, `remember_token`, `is_activate`, `created_at`, `updated_at`, `deleted_at`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`) VALUES
(1, 'System ', 'Admin', NULL, 'Admin', 'admin@admin.com', '9625874130', NULL, '98740', '2020-01-08', NULL, NULL, NULL, NULL, '$2y$10$2.Dvo8q0Lgkgl.jmXsYJleUFnndO0zQfgHTRiG6x14eR9oUF80kfe', NULL, NULL, 1, NULL, '2020-01-26 04:21:46', NULL, NULL, NULL, NULL, NULL),
(2, 'Teacher', 'Teacher 1', NULL, 'Teacher 1', 'teacher1@admin.com', '9625874130', NULL, '98740', '2020-01-08', NULL, NULL, NULL, NULL, '$2y$10$2.Dvo8q0Lgkgl.jmXsYJleUFnndO0zQfgHTRiG6x14eR9oUF80kfe', NULL, NULL, 1, NULL, '2020-01-26 04:21:46', NULL, NULL, NULL, NULL, NULL),
(3, 'Teacher', 'Teacher 2', NULL, 'Teacher 2', 'teacher2@admin.com', '9625874130', NULL, '98740', '2020-01-08', NULL, NULL, NULL, NULL, '$2y$10$2.Dvo8q0Lgkgl.jmXsYJleUFnndO0zQfgHTRiG6x14eR9oUF80kfe', NULL, NULL, 1, NULL, '2020-01-26 04:21:46', NULL, NULL, NULL, NULL, NULL),
(4, 'Student', 'Student 1', NULL, 'Student 1', 'student1@admin.com', '9625874130', NULL, '98740', '2020-01-08', NULL, NULL, NULL, NULL, '$2y$10$2.Dvo8q0Lgkgl.jmXsYJleUFnndO0zQfgHTRiG6x14eR9oUF80kfe', NULL, NULL, 1, NULL, '2020-01-26 04:21:46', NULL, NULL, NULL, NULL, NULL),
(5, 'Student', 'Student 2', NULL, 'Student 2', 'student2@admin.com', '9625874130', NULL, '98740', '2020-01-08', NULL, NULL, NULL, NULL, '$2y$10$2.Dvo8q0Lgkgl.jmXsYJleUFnndO0zQfgHTRiG6x14eR9oUF80kfe', NULL, NULL, 1, NULL, '2020-01-26 04:21:46', NULL, NULL, NULL, NULL, NULL),
(6, 'Student', 'Student 3', NULL, 'Student 3', 'student3@admin.com', '9625874130', NULL, '98740', '2020-01-08', NULL, NULL, NULL, NULL, '$2y$10$2.Dvo8q0Lgkgl.jmXsYJleUFnndO0zQfgHTRiG6x14eR9oUF80kfe', NULL, NULL, 1, NULL, '2020-01-26 04:21:46', NULL, NULL, NULL, NULL, NULL),
(7, 'Student', 'Student 4', NULL, 'Student 4', 'student4@admin.com', '9625874130', NULL, '98740', '2020-01-08', NULL, NULL, NULL, NULL, '$2y$10$2.Dvo8q0Lgkgl.jmXsYJleUFnndO0zQfgHTRiG6x14eR9oUF80kfe', NULL, NULL, 1, NULL, '2020-01-26 04:21:46', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `role_id` int UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, NULL, NULL),
(2, 2, 5, NULL, NULL, NULL),
(3, 3, 5, NULL, NULL, NULL),
(4, 7, 8, NULL, NULL, NULL),
(5, 6, 8, NULL, NULL, NULL),
(6, 5, 8, NULL, NULL, NULL),
(7, 4, 8, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academicsessions`
--
ALTER TABLE `academicsessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `academicsession_grade_subjects`
--
ALTER TABLE `academicsession_grade_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academicsession_grade_subject_1` (`grade_id`),
  ADD KEY `fk_academicsession_grade_subject_2` (`session_id`),
  ADD KEY `fk_academicsession_grade_subject_3` (`subject_id`);

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assignments_1_idx` (`classsession_id`),
  ADD KEY `fk_assignments_2_idx` (`session_id`),
  ADD KEY `fk_assignments_3_idx` (`grade_id`),
  ADD KEY `fk_assignments_4_idx` (`subject_id`),
  ADD KEY `fk_assignments_5_idx` (`teacher_id`);

--
-- Indexes for table `assignment_attachments`
--
ALTER TABLE `assignment_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assignment_attachment_1_idx` (`assignment_id`);

--
-- Indexes for table `assignment_students`
--
ALTER TABLE `assignment_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assignment_student_1_idx` (`assignment_id`),
  ADD KEY `fk_assignment_student_2_idx` (`student_id`);

--
-- Indexes for table `assignment_student_attachments`
--
ALTER TABLE `assignment_student_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_assignment_student_attachment_1_idx` (`assignment_student_id`);

--
-- Indexes for table `classsessions`
--
ALTER TABLE `classsessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_virtual_classes_1_idx` (`session_id`),
  ADD KEY `fk_virtual_classes_2_idx` (`grade_id`),
  ADD KEY `fk_virtual_classes_3_idx` (`subject_id`),
  ADD KEY `fk_virtual_classes_5_idx` (`teacher_id`);

--
-- Indexes for table `classsession_attendance_review`
--
ALTER TABLE `classsession_attendance_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_class_feedbacks_1_idx` (`classsession_id`),
  ADD KEY `fk_class_feedbacks_3_idx` (`student_id`);

--
-- Indexes for table `classsession_requests`
--
ALTER TABLE `classsession_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_request_virtual_classes_1_idx` (`grade_id`),
  ADD KEY `fk_request_virtual_classes_2_idx` (`session_id`),
  ADD KEY `fk_request_virtual_classes_3_idx` (`subject_id`),
  ADD KEY `fk_request_virtual_classes_5_idx` (`teacher_id`);

--
-- Indexes for table `classsession_request_students`
--
ALTER TABLE `classsession_request_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_request_virtual_class_student_1_idx` (`classsession_request_id`),
  ADD KEY `fk_request_virtual_class_student_2_idx` (`student_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_permission_role_1_idx` (`permission_id`),
  ADD KEY `fk_permission_role_2_idx` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`slug`);

--
-- Indexes for table `student_grades`
--
ALTER TABLE `student_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_grades_1_idx` (`student_id`),
  ADD KEY `fk_student_grades_2_idx` (`session_id`),
  ADD KEY `fk_student_grades_3_idx` (`grade_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `fk_subjects_1` (`grade_id`),
  ADD KEY `fk_subjects_2` (`parent_id`);

--
-- Indexes for table `subscriptionfees`
--
ALTER TABLE `subscriptionfees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subscription_fees_1_idx` (`session_id`),
  ADD KEY `fk_subscription_fees_2_idx` (`grade_id`);

--
-- Indexes for table `subscriptionfee_students`
--
ALTER TABLE `subscriptionfee_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subscriptionfee_student_1_idx` (`session_id`),
  ADD KEY `fk_subscriptionfee_student_2_idx` (`grade_id`),
  ADD KEY `fk_subscriptionfee_student_3_idx` (`subscription_fee_id`),
  ADD KEY `fk_subscriptionfee_student_4_idx` (`paid_by`),
  ADD KEY `fk_subscriptionfee_student_6_idx` (`student_id`);

--
-- Indexes for table `teacher_subjects`
--
ALTER TABLE `teacher_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_teacher_subjects_1` (`subject_id`),
  ADD KEY `fk_teacher_subjects_2` (`teacher_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_user_1_idx` (`user_id`),
  ADD KEY `fk_role_user_2_idx` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academicsessions`
--
ALTER TABLE `academicsessions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `academicsession_grade_subjects`
--
ALTER TABLE `academicsession_grade_subjects`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assignment_attachments`
--
ALTER TABLE `assignment_attachments`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assignment_students`
--
ALTER TABLE `assignment_students`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assignment_student_attachments`
--
ALTER TABLE `assignment_student_attachments`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classsessions`
--
ALTER TABLE `classsessions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `classsession_attendance_review`
--
ALTER TABLE `classsession_attendance_review`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classsession_requests`
--
ALTER TABLE `classsession_requests`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `classsession_request_students`
--
ALTER TABLE `classsession_request_students`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student_grades`
--
ALTER TABLE `student_grades`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subscriptionfees`
--
ALTER TABLE `subscriptionfees`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscriptionfee_students`
--
ALTER TABLE `subscriptionfee_students`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_subjects`
--
ALTER TABLE `teacher_subjects`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `academicsession_grade_subjects`
--
ALTER TABLE `academicsession_grade_subjects`
  ADD CONSTRAINT `fk_academicsession_grade_subject_1` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_academicsession_grade_subject_2` FOREIGN KEY (`session_id`) REFERENCES `academicsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_academicsession_grade_subject_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `fk_assignments_1` FOREIGN KEY (`classsession_id`) REFERENCES `classsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignments_2` FOREIGN KEY (`session_id`) REFERENCES `academicsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignments_3` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignments_4` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignments_5` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assignment_attachments`
--
ALTER TABLE `assignment_attachments`
  ADD CONSTRAINT `fk_assignment_attachment_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assignment_students`
--
ALTER TABLE `assignment_students`
  ADD CONSTRAINT `fk_assignment_student_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_assignment_student_2` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assignment_student_attachments`
--
ALTER TABLE `assignment_student_attachments`
  ADD CONSTRAINT `fk_assignment_student_attachment_1` FOREIGN KEY (`assignment_student_id`) REFERENCES `assignment_students` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `classsessions`
--
ALTER TABLE `classsessions`
  ADD CONSTRAINT `fk_virtual_classes_1` FOREIGN KEY (`session_id`) REFERENCES `academicsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_virtual_classes_2` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_virtual_classes_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_virtual_classes_5` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `classsession_attendance_review`
--
ALTER TABLE `classsession_attendance_review`
  ADD CONSTRAINT `fk_class_feedbacks_1` FOREIGN KEY (`classsession_id`) REFERENCES `classsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_class_feedbacks_3` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `classsession_requests`
--
ALTER TABLE `classsession_requests`
  ADD CONSTRAINT `fk_request_virtual_classes_1` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_classes_2` FOREIGN KEY (`session_id`) REFERENCES `academicsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_classes_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_classes_5` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `classsession_request_students`
--
ALTER TABLE `classsession_request_students`
  ADD CONSTRAINT `fk_request_virtual_class_student_1` FOREIGN KEY (`classsession_request_id`) REFERENCES `classsession_requests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_request_virtual_class_student_2` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `fk_permission_role_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_permission_role_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `student_grades`
--
ALTER TABLE `student_grades`
  ADD CONSTRAINT `fk_student_grades_1` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_student_grades_2` FOREIGN KEY (`session_id`) REFERENCES `academicsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_student_grades_3` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `fk_subjects_1` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_subjects_2` FOREIGN KEY (`parent_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Constraints for table `subscriptionfees`
--
ALTER TABLE `subscriptionfees`
  ADD CONSTRAINT `fk_subscription_fees_1` FOREIGN KEY (`session_id`) REFERENCES `academicsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscription_fees_2` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subscriptionfee_students`
--
ALTER TABLE `subscriptionfee_students`
  ADD CONSTRAINT `fk_subscriptionfee_student_1` FOREIGN KEY (`session_id`) REFERENCES `academicsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscriptionfee_student_2` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscriptionfee_student_3` FOREIGN KEY (`subscription_fee_id`) REFERENCES `subscriptionfees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscriptionfee_student_4` FOREIGN KEY (`paid_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_subscriptionfee_student_6` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `teacher_subjects`
--
ALTER TABLE `teacher_subjects`
  ADD CONSTRAINT `fk_teacher_subjects_1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_teacher_subjects_2` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_role_user_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_role_user_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
